import React, { Component } from 'react'
import { Route } from 'react-router-dom'
// components

import Registration from './components/Registration'
import './App.css'


class App extends Component {
  
  
  render() {
    return (
      <div className="App">
       
        <Route
          path="/Registration"
          render={() =>
            <Registration />}
        />
    
     
      </div>
      );
  }
}

export default App;
