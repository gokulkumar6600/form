import React from 'react';
import { shallow } from 'enzyme';
import Customerinfo from './customerinfo';

describe('<Customerinfo />', () => {
  test('renders', () => {
    const wrapper = shallow(<Customerinfo />);
    expect(wrapper).toMatchSnapshot();
  });
});
