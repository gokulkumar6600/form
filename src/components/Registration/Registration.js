import React, { Children } from "react";
import { CountryDropdown, RegionDropdown } from 'react-country-region-selector';

import axios from 'axios';
import NumberFormat from 'react-number-format';
import Header from '../Header/Header'
import Footer from '../Footer/Footer'

import { NotificationContainer, NotificationManager } from 'react-notifications';
import 'react-notifications/lib/notifications.css';

class Registration extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
          id: '',
          Fullname: '',
          Gender:'',
          Contact:'',
          Country: '',
          Region: '',
          Companyname:'',
          Email:'',
          Jobtitle:'',
          Yearsofexperience:'',
          AgreementText:'',
          formActivePanel3: 1,
          formActivePanel1Changed: false
        };
        this.onSubmit = this
            .onSubmit
            .bind(this);
      
    
    }
    
    selectCountry(val) {
        this.setState({ Country: val });
    }
    selectRegion(val) {
        this.setState({ Region: val });
    }
    
    handleChange = (e) => {

 
        this.setState({
            [e.target.name]: e.target.value
            
        })
    }



    
    onSubmit = (e) => {
        e.preventDefault()
        var str = this.state.docs;
        
        var re = Number(str.substring(3)) + 1001;
        const d =  String(re);
        const id = d;
        this.setState({ id })
        NotificationManager.success('Registered Sucessfully')
        setTimeout(() => window.location.reload(), 700);
      
        const form = {
          id:id,
          Fullname:this.state.Fullname,
          Gender:this.state.Gender,
          Contact:this.state.Contact,
          Country:this.state.Country,
          Region:this.state.Region,
          Companyname: this.state.Companyname,
          Jobtitle:this.state.Jobtitle,
          Email:this.state.Email,
          Yearsofexperience:this.state.Yearsofexperience,
      
        }

        const { Fullname, Gender,Contact,Region,Companyname,Jobtitle,Email,Yearsofexperience,AgreementText} = this.state;
     
        localStorage.setItem('Fullname',AgreementText ? Fullname : '');
        localStorage.setItem('Gender',AgreementText ? Gender : '');
        localStorage.setItem('Contact',AgreementText ? Contact : '');
        localStorage.setItem('Region',AgreementText ? Region : '');
        localStorage.setItem('Companyname',AgreementText ? Companyname : '');
        localStorage.setItem('Jobtitle',AgreementText ? Jobtitle : '');
        localStorage.setItem('Email',AgreementText ? Email : '');
        localStorage.setItem('Yearsofexperience',AgreementText ? Yearsofexperience : '');
        axios
            .post('http://localhost:8080/registration', form)
            .then(res => {
                
            if (res.data.some == "response") {
              NotificationManager.info("Registered successfully");
           
              setTimeout(() => window.location.reload(), 2000);
          } else {
        
          }
        })
          
    }



    
    swapFormActive = (a) => (param) => (e) => {
        this.setState({
            ['formActivePanel' + a]: param,
            ['formActivePanel' + a + 'Changed']: true
        });
    }
    handleNextPrevClick = (a) => (param) => (e) => {
        this.setState({
            ['formActivePanel' + a]: param,
            ['formActivePanel' + a + 'Changed']: true
        });
    }
    calculateAutofocus = (a) => {
        if (this.state['formActivePanel' + a + 'Changed']) {
            return true
        }
    }
   

    



    componentDidMount() {
        axios
            .get('http://localhost:8080/otp')
            .then(res => {
         console.log('gggg',res)
                let docs;
                if (res.data.docs.length > 0) {
                    docs = res.data.docs[0].id;
                } else {
                    docs = 'otp';
                }
                this.setState({ docs });
            })
    }
    render() {
  
        return (
            <div>

                <div class="page-wrap">
                    <Header />
                    <div>
                        <form onClick={this.onClick}>
                        <form onSubmit={this.onSubmit}>
                            <div className="container-fluid">
                                <div className="row justify-content-center">
                                    {this.state.formActivePanel3 === 1 && (
                                        <div className="container">
                                            <div className="card mb-3">
                                                <h3 className="font-weight-bold pt-3 pb-3 bg-light text-primary">Add Your Personal Details</h3>
                                               
                                                <div className="card-body">
                                                    <div className="row p-2">
                                                        <div className="col-md-3 text-right mt-2">
                                                            <label>Full Name</label>
                                                        </div>
                                                        <div className="col-md-4 text-left">
                                                            <input
                                                                className="form-control"
                                                                id="Fullname"
                                                                name="Fullname"
                                                                type="Fullname"
                                                                maxlength="50"
                                                                placeholder="Full name"
                                                                value={this.state.Fullname}
                                                                onChange={e => this.handleChange(e)} required></input>
                                                        </div>
                                                    </div>
                                                 <br></br>
                                                    <div className="row p-2">
                                                        <div className="col-md-3 text-right mt-2">
                                                            <label> Gender</label>
                                                        </div>
                                                        <div className="col-md-3 text-left">
                                                            <select
                                                                className="form-control"
                                                                id="Gender"
                                                                name="Gender"
                                                                type="Gender"
                                                                value={this.state.Gender}
                                                                onChange={e => this.handleChange(e)}required>
                                                                <option value="">Choose your Gender</option>
                                                                <option value="Male">Male</option>
                                                                <option value="Female">Female</option>
                                                                <option value="Others">Others</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <br></br>
                                                    <div className="row p-2">
                                                        <div className="col-md-3 text-right mt-2">
                                                            <label>
                                                                Country</label>
                                                        </div>
                                                        <div className="col-md-3 text-left">
                                                            <CountryDropdown
                                                                className="form-control"
                                                                value={this.state.Country}
                                                                onChange={(val) => this.selectCountry(val)} required/>
                                                        </div>
                                                        <div className="col-md-3 text-left">
                                                            <RegionDropdown
                                                                className="form-control"
                                                                country={this.state.Country}
                                                                value={this.state.Region}
                                                                onChange={(val) => this.selectRegion(val)}required />
                                                        </div>
                                                    </div>
                                                    <br></br>
                                                    <div className="row">
                                            <div className="col-md-3  text-right mt-2">
                                                <label>Contact No</label>
                                            </div>
                                            <div className="col-md-6 text-left">
                                                <NumberFormat
                                                    className="form-control"
                                                    placeholder="Contact"
                                                    required
                                                    value={this.state.Contact}
                                                    format="+91 (###) ###-####"
                                                    mask="_"
                                                    onValueChange={(values) => {
                                                        const { value } = values;
                                                        this.setState({ Contact: value })
                                                    }} />
                                            </div>
                                            
                                        </div>
                                                    <div className="row p-3 justify-content-right">
                                                        <div className="col-md-12">
                                                            <button
                                                                className="btn btn-primary button"
                                                                disabled={!this.state.Fullname,
                                                                    !this.state.Gender,
                                                                    !this.state.Country,
                                                                    !this.state.Region,
                                                                    !this.state.Contact
                                                                    }
                                                                onClick={this.handleNextPrevClick(3)(2)}>
                                                                <span>
                                                                    NEXT
                                                                </span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    )}
                                    {this.state.formActivePanel3 === 2 && (
                                        <div className="container">
                                            <div className="card mb-3">
                                            <div className="card mb-3">
                                                <h3 className="font-weight-bold pt-3 pb-3 bg-light text-primary">Add Your Company Details</h3>
                                                <div class="card-body">
                                                <div className="row p-2">
                                                        <div className="col-md-3 text-right mt-2">
                                                            <label>Company Name</label>
                                                        </div>
                                                        <div className="col-md-4 text-left">
                                                            <input
                                                                className="form-control"
                                                                id="Companyname"
                                                                name="Companyname"
                                                                type="Companyname"
                                                                required
                                                                maxlength="50"
                                                                placeholder="Company name"
                                                                value={this.state.Companyname}
                                                                onChange={e => this.handleChange(e)}></input>
                                                        </div>
                                                    </div>
                                                    <br></br>
                                                    <div className="row p-2">
                                                        <div className="col-md-3 text-right mt-2">
                                                            <label>Email</label>
                                                        </div>
                                                        <div className="col-md-4 text-left">
                                                            <input
                                                                className="form-control"
                                                                id="Email"
                                                                name="Email"
                                                                type="Email"
                                                                maxlength="50"
                                                                required
                                                                placeholder="Email"
                                                                value={this.state.Email}
                                                                onChange={e => this.handleChange(e)}></input>
                                                        </div>
                                                    </div>
                                                    <br></br>
                                                    <div className="row p-2">
                                                        <div className="col-md-3 text-right mt-2">
                                                            <label>Job Title</label>
                                                        </div>
                                                        <div className="col-md-6 text-left">
                                                            <textarea
                                                                className="form-control"
                                                                id="Jobtitle"
                                                                name="Jobtitle"
                                                                type="Jobtitle"
                                                                maxlength="60"
                                                                required
                                                                placeholder="Jobtitle"
                                                                value={this.state.Jobtitle}
                                                                onChange={e => this.handleChange(e)}></textarea>
                                                        </div>
                                                    </div>
                                                    <div className="row p-2">
                                                        <div className="col-md-3 text-right mt-2">
                                                            <label>Years of Experience</label>
                                                        </div>
                                                        <div className="col-md-4 text-left">
                                                        <input
                                                                type="number"
                                                                className="form-control"
                                                                name="Yearsofexperience"
                                                                id="Yearsofexperience"
                                                                required
                                                                value={this.state.Yearsofexperience}
                                                                onChange={e => this.handleChange(e)}></input>
                                                        </div>
                                                    </div>
                                                    <br></br>
                                                    <div className="row p-2">
                              
                                <div className="col-md-3 text-right mt-2">
                                    <input
                                        type="checkbox"
                                        className="form-check-input"
                                        required
                                        id="AgreementText"
                                        name="AgreementText"
                                        checked={this.state.AgreementText}
                                        onChange={e => this.handleChange(e)}
                                       ></input>
                                </div>
                                <div className="col-md-3">
                                    <p className="style">
                                        I accept the Terms and Conditions
                                    </p>
                                </div>
                            </div>

                                                    <div className="row p-3">
                                                        <div className="col-md-12">
                                                            <button
                                                                color="mdb-color"
                                                                rounded
                                                                className="float-left btn btn-primary button"
                                                                onClick={this.handleNextPrevClick(3)(1)}>
                                                                <div>
                                                                    BACK
                                                                </div>
                                                            </button>
                                                            <button
                                                              
                                                                color="mdb-color"
                                                                rounded
                                                                className="float-right btn btn-primary button"
                                                                disabled={!this.state.Companyname,
                                                                    !this.state.Email,                                                                    !this.state.Country,
                                                                    !this.state.Jobtitle,
                                                                    !this.state.Yearsofexperience
                                                                    }
                                                                onClick={this.handleNextPrevClick(3)(3)}>
                                                                <span>
                                                                NEXT
                                                                </span>
                                                            </button>
                                                            
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    )}
                                    {this.state.formActivePanel3 === 3 && (
                                        <div className="container">
                                            <div className="card mb-2 shadow-sm p-3">
                                                Enter Your OTP
                                                <div className="row p-2">
                                                    <div className="col-md-12">
                                                        <button
                                                            color="mdb-color"
                                                            rounded
                                                            className="float-left btn btn-primary button"
                                                            onClick={this.handleNextPrevClick(3)(2)}>
                                                            <div>
                                                                BACK
                                                            </div>
                                                        </button>
                                                        <button
                                                            color="mdb-color"
                                                            rounded
                                                            className="float-right btn btn-primary button"
                                                            onClick={this.handleNextPrevClick(3)(3)}>
                                                            <span>
                                                               VERIFY
                                                            </span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <NotificationContainer />
                                        </div>
                                    )}
                                  
                                </div>
                            </div>
                            </form>
                        </form>
                    </div>
                </div>
                <Footer />
            </div>
        );
    };
}

export default Registration;