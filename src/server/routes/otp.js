const express = require('express')
const router = express.Router()
const Registration = require('../database/models/registration')

// for front end
router.get("/", (req, res) => {
    Registration.find({}).sort({ _id: -1 }).limit(1).exec(function (err, docs) {
      console.log('docs',docs);
      
      
    if (err) {
      return res.status(500).json({ err })
    }
    else {
      return res.status(200).json({ docs })
    }
  });
})
module.exports = router