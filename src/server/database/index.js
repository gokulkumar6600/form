const mongoose = require('mongoose')
mongoose.Promise = global.Promise
var MongoClient = require('mongodb').MongoClient;
const uri = 'mongodb://localhost:27017/ForQQ' 

mongoose.connect(uri).then(
    () => { 
        console.log('Connected to Mongo');
        
    },
    err => {
         console.log('error connecting to Mongo: ')
         console.log(err);
         
        }
  );
module.exports = mongoose.connection