const mongoose = require('mongoose')
const Schema = mongoose.Schema
mongoose.promise = Promise

const RegistrationSchema = new Schema({
	id: String,
	Fullname: String,
	Gender: String,
	Contact: String,
	Country: String,
	Region: String,
	Companyname: String,
	Email: String,
	Jobtitle: String,
	Yearsofexperience: String,
	AgreementText: String,

})


const Registration = mongoose.model('Registration', RegistrationSchema)
module.exports = Registration
    